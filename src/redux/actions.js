import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithPopup,
  sendPasswordResetEmail,
  updatePassword,
  signOut,
} from 'firebase/auth';
import { toast } from 'react-toastify';
import auth from '../firebase';
import { userConstants } from './actionTypes';

const loginRequest = () => ({
  type: userConstants.LOGIN_USER_REQUEST,
});

const loginSuccess = (user) => ({
  type: userConstants.LOGIN_USER_SUCCESS,
  payload: user,
});

const loginError = (error) => ({
  type: userConstants.LOGIN_USER_ERROR,
  payload: error,
});

const logOutRequest = () => ({
  type: userConstants.LOGOUT_USER_REQUEST,
});

const logOutSuccess = () => ({
  type: userConstants.LOGOUT_USER_SUCCESS,
});

const logOutError = (error) => ({
  type: userConstants.LOGOUT_USER_ERROR,
  payload: error,
});

export const clean = () => ({
  type: userConstants.CLEAN_UP,
});

const signUpRequest = () => ({
  type: userConstants.SIGNUP_USER_REQUEST,
});

const signUpSuccess = (user) => ({
  type: userConstants.SIGNUP_USER_SUCCESS,
  payload: user,
});

const signUpError = (error) => ({
  type: userConstants.SIGNUP_USER_ERROR,
  payload: error,
});

const googleSignInRequest = () => ({
  type: userConstants.LOGIN_GOOGLE_REQUEST,
});

const googleSignInSuccess = (user) => ({
  type: userConstants.LOGIN_GOOGLE_SUCCESS,
  payload: user,
});

const googleSignInError = (error) => ({
  type: userConstants.LOGIN_GOOGLE_ERROR,
  payload: error,
});

const recoverPasswordRequest = () => ({
  type: userConstants.RECOVERY_REQUEST,
});

const recoverPasswordSuccess = () => ({
  type: userConstants.RECOVERY_SUCCESS,
});

const recoverPasswordError = (error) => ({
  type: userConstants.RECOVERY_ERROR,
  payload: error,
});

const updatePasswordRequest = () => ({
  type: userConstants.RECOVERY_REQUEST,
});

const updatePasswordSuccess = () => ({
  type: userConstants.RECOVERY_SUCCESS,
});

const updatePasswordError = (error) => ({
  type: userConstants.RECOVERY_ERROR,
  payload: error,
});

// eslint-disable-next-line import/prefer-default-export
export const loginInitiate = (email, password) => (dispatch) => {
  dispatch(loginRequest());
  signInWithEmailAndPassword(auth, email, password)
    .then(({ user }) => {
      dispatch(loginSuccess(user));
      toast.success('Вход выполнен успешно!');
    })
    .catch((error) => dispatch(loginError(error.message)));
};

export const signUpInitiate = (email, password) => (dispatch) => {
  dispatch(signUpRequest());
  createUserWithEmailAndPassword(auth, email, password)
    .then(({ user }) => {
      dispatch(signUpSuccess(user));
      toast.success('Регистрация прошла успешно!');
    })
    .catch((error) => dispatch(signUpError(error.message)));
};

export const googleLoginInitiate = () => (dispatch) => {
  const provider = new GoogleAuthProvider();
  dispatch(googleSignInRequest());
  signInWithPopup(auth, provider)
    .then(({ user }) => {
      dispatch(googleSignInSuccess(user));
      toast.success('Вход выполнен успешно!');
    })
    .catch((error) => dispatch(googleSignInError(error.message)));
};

export const recoverPassword = (email) => (dispatch) => {
  dispatch(recoverPasswordRequest());
  sendPasswordResetEmail(auth, email)
    .then(() => {
      dispatch(recoverPasswordSuccess());
      toast.success('Отправка выполнена успешно!');
    })
    .catch((error) => dispatch(recoverPasswordError(error.message)));
};

export const editPassword = (user, newPassword) => (dispatch) => {
  dispatch(updatePasswordRequest());
  updatePassword(user, newPassword)
    .then(() => {
      dispatch(updatePasswordSuccess());
      toast.success('Пароль успешно обновлён');
    })
    .catch((error) => dispatch(updatePasswordError(error.message)));
};

export const logOutInitiate = () => (dispatch) => {
  dispatch(logOutRequest());
  signOut(auth).then(() => {
    dispatch(logOutSuccess());
    toast.success('Выход выполнен успешно!');
  }).catch((error) => dispatch(logOutError(error.message)));
};
