import { userConstants } from './actionTypes';

const initialState = {
  loading: false,
  currentUser: null,
  error: null,
  isLogged: null,
};

// eslint-disable-next-line default-param-last
const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case userConstants.SIGNUP_USER_REQUEST:
    case userConstants.LOGIN_USER_REQUEST:
    case userConstants.LOGIN_GOOGLE_REQUEST:
    case userConstants.RECOVERY_REQUEST:
    case userConstants.UPDATE_PASSWORD_REQUEST:
    case userConstants.LOGOUT_USER_REQUEST:
      return { ...state, loading: true };
    case userConstants.SIGNUP_USER_SUCCESS:
    case userConstants.LOGIN_USER_SUCCESS:
    case userConstants.LOGIN_GOOGLE_SUCCESS:
      return {
        ...state, currentUser: payload, error: null, loading: false, isLogged: true,
      };
    case userConstants.SIGNUP_USER_ERROR:
    case userConstants.LOGIN_USER_ERROR:
    case userConstants.RECOVERY_ERROR:
    case userConstants.LOGIN_GOOGLE_ERROR:
    case userConstants.UPDATE_PASSWORD_ERROR:
    case userConstants.LOGOUT_USER_ERROR:
      return { ...state, loading: false, error: payload };
    case userConstants.CLEAN_UP:
      return { ...state, loading: false, error: null };
    case userConstants.LOGOUT_USER_SUCCESS:
    case userConstants.RECOVERY_SUCCESS:
    case userConstants.UPDATE_PASSWORD_SUCCESS:
      return {
        ...state, currentUser: null, error: null, loading: false, isLogged: false,
      };
    default:
      return state;
  }
};

export default userReducer;
