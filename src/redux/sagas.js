import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import LoginPage from '../components/AUTH/loginPage';
import { userConstants } from './actionTypes';
// import firebaseDb from '../firebase';

export function* loginSaga(action) {
  try {
    const payload = yield call(LoginPage, action.email, action.password);
    yield put({ type: userConstants.LOGIN_USER_SUCCESS, payload });
  } catch (error) {
    yield put({ type: userConstants.LOGIN_USER_ERROR, error });
  }
}

export function* watchLogin() {
  yield takeLatest(userConstants.LOGIN_USER_REQUEST, loginSaga);
}

export default function* rootSaga() {
  yield all([
    watchLogin(),
  ]);
}
