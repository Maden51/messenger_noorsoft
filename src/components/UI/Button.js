/* eslint-disable react/prop-types,react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  width: ${({ contain }) => (contain ? 'auto' : '100%')};
  outline: none;
  padding: 1rem 4rem;
  border-radius: 2rem;
  font-size: 1.2rem;
  color: white;
  cursor: pointer;
  font-weight: 700;
  background-color: ${({ color }) => {
    if (color === 'red') return 'red';
    if (color === 'main') return 'grey';
    return 'blue';
  }};
  margin: 1rem 0 2rem 0;
  border: none;
  transition: all 0.2s;
  &:hover {
    transform: translateY(-3px);
  }
  &:active {
    transform: translateY(2px);
  }
  &:disabled {
    cursor: not-allowed;
    background-color: #333;
  }`;

function Button(
  {
    children, disabled, loading, contain, color, ...rest
  },
) {
  return (
    <StyledButton color={color} contain={contain} disabled={disabled} {...rest}>
      {loading || children}
    </StyledButton>
  );
}

export default Button;
