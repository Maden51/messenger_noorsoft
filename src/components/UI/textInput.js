import React from 'react';
import { useField } from 'formik';
import { Input, Label } from 'reactstrap';

// eslint-disable-next-line react/prop-types
function TextInput({ label, ...props }) {
  const [field, meta] = useField(props);
  return (
    <>
      {/* eslint-disable-next-line react/prop-types */}
      <Label htmlFor={props.id || props.name}>{label}</Label>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <Input {...field} {...props} />
      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
    </>
  );
}

export default TextInput;
