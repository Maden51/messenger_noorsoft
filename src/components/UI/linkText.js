/* eslint-disable react/prop-types,react/jsx-props-no-spreading */
import React from 'react';
import { NavItem, Nav } from 'reactstrap';

function LinkText(
  { children },
) {
  return (
    <Nav>
      <NavItem>
        {children}
      </NavItem>
    </Nav>
  );
}

export default LinkText;
