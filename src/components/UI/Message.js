import React from 'react';
import styled from 'styled-components';

const P = styled.p`
  font-weight: 700;
  font-size: 0.8rem;
  margin: 0;
  color: ${({ error, success }) => {
    if (error) return 'red';
    if (success) return 'green';
    return 'blue';
  }};
  opacity: ${({ show }) => (show ? 1 : 0)};
  visibility: ${({ show }) => (show ? 'visible' : 'hidden')};
  transition: all 0.3s;
`;

function Message({
  // eslint-disable-next-line react/prop-types
  children, error, success, show,
}) {
  return (
    <P error={error} success={success} show={show}>
      { children }
    </P>
  );
}

export default Message;
