/* eslint-disable react/prop-types,react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import { Button } from 'reactstrap';
import { useNavigate } from 'react-router-dom';

function Header({
  children, logout, loading, currentUser, ...rest
}) {
  const navigate = useNavigate();
  useEffect(() => {
    if (currentUser === null) {
      navigate('/login');
    }
  }, [currentUser, navigate]);
  return (
    <header className="homePage-header">
      <h2 className="user-email" {...rest}>{ currentUser?.email || children }</h2>
      <Button color="primary" onClick={logout} loading={loading ? 'Выход...' : null}>Выйти</Button>
    </header>
  );
}

export default Header;
