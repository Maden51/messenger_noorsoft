import React from 'react';

function Aside() {
  return (
    <aside className="homePage-aside">
      <div className="card card-active">
        <i className="fa-solid fa-comments fa-2xl" />
        <h6 className="card-title">Активные</h6>
      </div>
      <div className="card card-closed">
        <i className="fa-solid fa-comment-slash fa-2xl" />
        <h6 className="card-title">Завершённые</h6>
      </div>
      <div className="card card-saved">
        <i className="fa-solid fa-hard-drive fa-2xl" />
        <h6 className="card-title">Сохранённые</h6>
      </div>
    </aside>
  );
}

export default Aside;
