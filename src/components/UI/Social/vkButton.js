/* eslint-disable react/prop-types,react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';

const StyledVkButton = styled.button`
  outline: none;
  border-radius: 5px;
  color: white;
  cursor: pointer;
  background-color: blue;
  border: none;
  padding: 20px;
  transition: all 0.2s;
  &:hover {
    transform: translateY(-3px);
  }
  &:active {
    transform: translateY(2px);
  }
`;
function VkButton({ children, onClick, ...rest }) {
  return (
    <StyledVkButton onClick={onClick} {...rest}>
      { children }
    </StyledVkButton>
  );
}

export default VkButton;
