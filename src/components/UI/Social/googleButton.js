/* eslint-disable react/prop-types,react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';

const StyledGoogleButton = styled.button`
  outline: none;
  border-radius: 5px;
  color: white;
  cursor: pointer;
  background-color: orange;
  border: none;
  padding: 20px;
  transition: all 0.2s;
  &:hover {
    transform: translateY(-3px);
  }
  &:active {
    transform: translateY(2px);
  }
`;
function GoogleButton({ children, onClick, ...rest }) {
  return (
    <StyledGoogleButton onClick={onClick} {...rest}>
      { children }
    </StyledGoogleButton>
  );
}

export default GoogleButton;
