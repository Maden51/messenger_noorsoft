/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import '../../css/loginPage.css';
import { Link, useNavigate } from 'react-router-dom';

import {
  FormGroup,
} from 'reactstrap';
import TextInput from '../UI/textInput';
import Button from '../UI/Button';
import * as actions from '../../redux/actions';
import Message from '../UI/Message';
import LinkText from '../UI/linkText';
import GoogleButton from '../UI/Social/googleButton';
import VkButton from '../UI/Social/vkButton';

function LoginPage({
  login, loading, error, cleanUp, google, currentUser,
}) {
  const navigate = useNavigate();

  useEffect(() => {
    if (currentUser) {
      navigate('/home');
    }
  }, [currentUser, navigate]);

  useEffect(() => () => {
    cleanUp();
  }, [cleanUp]);
  return (
    <div className="loginPage-wrapper">
      <h1 className="loginPage__title title">Авторизация</h1>
      <Formik
        initialValues={{
          email: '',
          password: '',
        }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email('Неверный ввод почты')
            .required('Необходим'),
          password: Yup.string()
            .required('Необходим')
            .min(8, 'В пароле должно быть минимум 8 символов'),
        })}
        onSubmit={async (values, { setSubmitting }) => {
          await login(values.email, values.password);
          setSubmitting(false);
        }}
      >
        <Form>
          <FormGroup>
            <i className="fas fa-envelope-square" />
            <TextInput
              label="Email"
              name="email"
              type="email"
              placeholder="Введите вашу почту"
              required
            />
          </FormGroup>
          <FormGroup>
            <i className="fas fa-lock" />
            <TextInput
              label="Пароль"
              name="password"
              type="password"
              placeholder="Введите пароль"
              required
            />
          </FormGroup>
          <Button
            loading={loading ? 'Вход...' : null}
            type="submit"
          >
            Вход
          </Button>
          <Message error show={error}>
            {error}
          </Message>
          <div className="socialLogin">
            <GoogleButton type="button" onClick={google}>
              <i className="fa-brands fa-google fa-lg" />
            </GoogleButton>
            <VkButton type="button">
              <i className="fa-brands fa-vk fa-xl" />
            </VkButton>
          </div>
          <div className="linksBox">
            <Link to="/register">
              <LinkText>Регистрация</LinkText>
            </Link>
            <Link to="/recoverPassword">
              <LinkText>Забыли пароль?</LinkText>
            </Link>
          </div>
        </Form>
      </Formik>
    </div>
  );
}

const mapStateToProps = ({ user }) => ({
  loading: user.loading,
  error: user.error,
  currentUser: user.currentUser,
});

const mapDispatchToProps = ({
  login: actions.loginInitiate,
  google: actions.googleLoginInitiate,
  cleanUp: actions.clean,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPage);
