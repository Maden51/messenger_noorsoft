/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import '../../css/loginPage.css';

import { Link, useNavigate } from 'react-router-dom';
import TextInput from '../UI/textInput';
import Button from '../UI/Button';
import * as actions from '../../redux/actions';
import Message from '../UI/Message';
import LinkText from '../UI/linkText';

function SignUpPage({
  signUp, loading, error, cleanUp, currentUser,
}) {
  const navigate = useNavigate();

  useEffect(() => {
    if (currentUser) {
      navigate('/home');
    }
  }, [currentUser, navigate]);

  useEffect(() => () => {
    cleanUp();
  }, [cleanUp]);
  return (
    <div className="signupPage-wrapper">
      <h1 className="signUp__title title">Регистрация</h1>
      <Formik
        initialValues={{
          email: '',
          password: '',
          passwordConfirm: '',
        }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email('Неверный ввод почты')
            .required('Необходим'),
          password: Yup.string()
            .required('Необходим')
            .min(8, 'В пароле должно быть минимум 8 символов')
            .matches(/\d/, 'В пароле должна быть минимум одна цифра')
            .matches(/[a-zа-я]/, 'В пароле должна быть минимум одна маленькая буква')
            .matches(/[A-ZА-Я]/, 'В пароле должна быть минимум одна большая буква'),
          passwordConfirm: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Пароли не совпадают')
            .required('Повторите пароль')
          ,
        })}
        onSubmit={async (values, { setSubmitting }) => {
          await signUp(values.email, values.password);
          setSubmitting(false);
        }}
      >
        <Form>
          <i className="fas fa-envelope-square" />
          <TextInput
            label="Email"
            name="email"
            type="email"
            placeholder="Введите вашу почту"
            required
          />
          <i className="fas fa-lock" />
          <TextInput
            label="Пароль"
            name="password"
            type="password"
            placeholder="Введите пароль"
            required
          />
          <i className="fas fa-lock" />
          <TextInput
            label="Подтверждение пароля"
            name="passwordConfirm"
            type="passwordConfirm"
            placeholder="Повторите пароль"
            required
          />
          <Button
            loading={loading ? 'Регистрация...' : null}
            type="submit"
          >
            Регистрация
          </Button>
          <Message error show={error}>
            {error}
          </Message>
          <div className="linksBox">
            <Link to="/login">
              <LinkText>Войти</LinkText>
            </Link>
            <Link to="/recoverPassword">
              <LinkText>Забыли пароль?</LinkText>
            </Link>
          </div>
        </Form>
      </Formik>
    </div>
  );
}

const mapStateToProps = ({ user }) => ({
  loading: user.loading,
  error: user.error,
  currentUser: user.currentUser,
});

const mapDispatchToProps = ({
  signUp: actions.signUpInitiate,
  cleanUp: actions.clean,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUpPage);
