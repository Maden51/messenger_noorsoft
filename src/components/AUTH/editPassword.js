/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { Formik, Form } from 'formik';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Link, useNavigate } from 'react-router-dom';
import * as actions from '../../redux/actions';
import TextInput from '../UI/textInput';
import Button from '../UI/Button';
import Message from '../UI/Message';
import LinkText from '../UI/linkText';
import '../../css/forgetPassword.css';

function EditPassword({
  updatePassword, cleanUp, loading, error, currentUser,
}) {
  const navigate = useNavigate();

  useEffect(() => {
    if (currentUser) {
      navigate('/login');
    }
  }, [currentUser, navigate]);

  useEffect(() => () => {
    cleanUp();
  }, [cleanUp]);
  return (
    <div className="editPass-wrapper">
      <h1 className="editPassword__title title">Обновить пароль</h1>
      <Formik
        initialValues={{
          password: '',
          confirmPassword: '',
        }}
        validationSchema={Yup.object({
          password: Yup.string()
            .required('Необходим')
            .min(8, 'В пароле должно быть минимум 8 символов')
            .matches(/\d/, 'В пароле должна быть минимум одна цифра')
            .matches(/[a-zа-я]/, 'В пароле должна быть минимум одна маленькая буква')
            .matches(/[A-ZА-Я]/, 'В пароле должна быть минимум одна большая буква'),
          passwordConfirm: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Пароли не совпадают')
            .required('Повторите пароль')
          ,
        })}
        onSubmit={async (values, { setSubmitting }) => {
          await updatePassword(currentUser, values.password);
          setSubmitting(false);
        }}
      >
        <Form>
          <i className="fas fa-lock" />
          <TextInput
            label="Пароль"
            name="password"
            type="password"
            placeholder="Введите новый пароль"
            required
          />
          <i className="fas fa-lock" />
          <TextInput
            label="Подтверждение пароля"
            name="passwordConfirm"
            type="password"
            placeholder="Повторите пароль"
            required
          />
          <Button
            type="submit"
            loading={loading ? 'Отправка...' : null}
          >
            Подтвердить
          </Button>
          <div className="messagesBox">
            <Message error show={error}>
              {error}
            </Message>
          </div>
          <div className="linksBox">
            <Link to="/login">
              <LinkText>Войти</LinkText>
            </Link>
            <Link to="/register">
              <LinkText>Зарегистрироваться</LinkText>
            </Link>
          </div>
        </Form>
      </Formik>
    </div>
  );
}

const mapStateToProps = ({ user }) => ({
  loading: user.loading,
  error: user.error,
  currentUser: user.currentUser,
});

const mapDispatchToProps = ({
  cleanUp: actions.clean,
  updatePassword: actions.editPassword,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditPassword);
