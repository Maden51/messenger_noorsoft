/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { Formik, Form } from 'formik';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import * as actions from '../../redux/actions';
import TextInput from '../UI/textInput';
import Button from '../UI/Button';
import Message from '../UI/Message';
import LinkText from '../UI/linkText';
import '../../css/forgetPassword.css';

function ForgetPassword({
  sendEmail, cleanUp, loading, error,
}) {
  useEffect(() => () => {
    cleanUp();
  }, [cleanUp]);
  return (
    <div className="forgetPass-wrapper">
      <h1 className="recover__title title">Восстановление пароля</h1>
      <Formik
        initialValues={{
          email: '',
        }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email('Неверный ввод почты')
            .required('Необходимо'),
        })}
        onSubmit={async (values, { setSubmitting }) => {
          await sendEmail(values.email);
          setSubmitting(false);
        }}
      >
        <Form>
          <i className="fas fa-envelope-square" />
          <TextInput
            label="Email"
            name="email"
            type="email"
            placeholder="Введите вашу почту"
            required
          />
          <Button
            type="submit"
            loading={loading ? 'Отправка...' : null}
          >
            Отправить
          </Button>
          <div className="messagesBox">
            <Message error show={error}>
              {error}
            </Message>
          </div>
          <div className="linksBox">
            <Link to="/login">
              <LinkText>Войти</LinkText>
            </Link>
            <Link to="/register">
              <LinkText>Зарегистрироваться</LinkText>
            </Link>
          </div>
        </Form>
      </Formik>
    </div>
  );
}

const mapStateToProps = ({ user }) => ({
  loading: user.loading,
  error: user.error,
});

const mapDispatchToProps = ({
  cleanUp: actions.clean,
  sendEmail: actions.recoverPassword,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgetPassword);
