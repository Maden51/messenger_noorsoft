/* eslint-disable react/prop-types */
import React from 'react';
import '../../css/homePage.css';
import { connect } from 'react-redux';

import * as actions from '../../redux/actions';
import Aside from '../UI/Main/Aside';
import Header from '../UI/Main/Header';

function HomePage({
  currentUser, loading, logOut,
}) {
  return (
    <div className="homePage-wrapper">
      <Header
        currentUser={currentUser}
        loading={loading}
        logout={logOut}
      >
        Email
      </Header>
      <div className="homePage-content">
        <Aside />
        <main className="blank" />
      </div>
    </div>
  );
}

const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser,
  loading: user.loading,
  isLogged: user.isLogged,
});

const mapDispatchToProps = ({
  logOut: actions.logOutInitiate,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
// eslint-disable-next-line react-redux/prefer-separate-component-file
)(HomePage);
