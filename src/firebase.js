import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';

const firebaseConfig = {
  apiKey: 'AIzaSyDTzwCCNNCGqyjhdtCSjP3_2xc5gpkL4uk',
  authDomain: 'wehelp-messenger.firebaseapp.com',
  databaseURL: 'https://wehelp-messenger-default-rtdb.europe-west1.firebasedatabase.app/',
  projectId: 'wehelp-messenger',
  storageBucket: 'wehelp-messenger.appspot.com',
  messagingSenderId: '727915534285',
  appId: '1:727915534285:web:ddbccbb6016b4cc44d1684',
  measurementId: 'G-G4K2LN2FR8',
};

const app = initializeApp(firebaseConfig);
export const db = getDatabase(app);

const auth = getAuth(app);

export default auth;
