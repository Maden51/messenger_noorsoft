/* eslint-disable react/prop-types */
import React from 'react';
import '../css/App.css';
import {
  BrowserRouter, Routes, Route, Navigate,
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import LoginPage from '../components/AUTH/loginPage';
import HomePage from '../components/MESSENGER/homePage';
import SignUpPage from '../components/AUTH/signUpPage';
import ForgetPassword from '../components/AUTH/forgetPassword';
import EditPassword from '../components/AUTH/editPassword';

function App({ isLogged }) {
  return (
    <BrowserRouter>
      <div className="app-container">
        <Routes>
          <Route
            exact
            path="*"
            element={(
              isLogged
                ? <Navigate to="/home" />
                : <Navigate to="/login" />
            )}
          />
          <Route exact path="/login" element={<LoginPage />} />
          <Route exact path="/home" element={<HomePage />} />
          <Route exact path="/register" element={<SignUpPage />} />
          <Route exact path="/recoverPassword" element={<ForgetPassword />} />
          <Route exact path="/editPassword" element={<EditPassword />} />
        </Routes>
        <ToastContainer
          autoClose={2000}
          position="top-right"
          draggable
          pauseOnHover
          pauseOnFocusLoss
          closeOnClick
        />
      </div>
    </BrowserRouter>
  );
}

const mapStateToProps = ({ user }) => ({
  isLogged: user.isLogged,
});

export default connect(
  mapStateToProps,
  null,
)(App);
